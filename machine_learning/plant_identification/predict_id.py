from PIL import Image
import json
import torch
import torch.nn as nn
from torchvision import transforms, models
from argparse import ArgumentParser
import os


def load_model(model_path):
    model = models.densenet161()
    model.classifier = nn.Linear(2208, 1081)  # Update if model/dataset changes
    model.load_state_dict(torch.load(model_path, map_location='cpu'))
    model.eval()
    return model


def load_reference(path):
    with open(path, 'r') as f:
        labels = json.load(f)
    return labels


def predict(img_path, model, labels):
    image = Image.open(img_path)
    test_transform = transforms.Compose([transforms.Resize((299, 299)), transforms.ToTensor()])
    y_result = model(test_transform(image).unsqueeze(0))
    # torch.max returns both max and argmax
    probs = torch.nn.functional.softmax(y_result, dim=1)
    confidence_level, result_idx = torch.max(probs, 1)
    result_idx = str(result_idx.item())
    return labels[result_idx], float(confidence_level)


def get_plant_prediction(img_path):
    script_location = (os.path.dirname(os.path.abspath(__file__)))
    model_path = os.path.join(script_location, "Models/plant_id_model_3epochs.pth")
    labels = load_reference(os.path.join(script_location, "labels.json"))
    model = load_model(model_path)
    return predict(img_path, model, labels)


def main():
    img_path = "Datasets/test_img_1355936.jpg"
    result, confidence = get_plant_prediction(img_path)
    print(result)
    print(confidence)


if __name__ == "__main__":
    main()
