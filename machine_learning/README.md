# Machine Learning Model

## Requirements

* python 3.7 - 3.9
* install pip modules in the requirements.txt

## Usage

To predict an image run the below command with a path to an image that you want to predict.

```bat
 python .\predict.py --path "YOUR/PATH/HERE"
 
```
